import styled from "styled-components";


export const Container = styled.main `
    width: 100vw;
    min-height: 100vh;
`

export const InfoText = styled.div `
    width: 100vw;
    height: 200px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    h2{
        font-family: var(--fontTitle);
        font-size: 2.0rem;
        margin-left: 5vw;
    }
    p{
        width: 35vw;
        text-align: left;
        font-family: var(--fontSubtitle);
        margin-right: 5vw;
    }
`