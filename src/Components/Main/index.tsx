import React from 'react'
import {Container, InfoText} from './styles'

export default function Main(){
    return (
        <>
        <Container>
            <InfoText>
                <h2>Sobre mim</h2>
                <p>um pouco sobre quem eu sou, posso dizer que é mais curiosidades sobre mim, onde moro, o que gosto de fazer e por ai vai!</p>
            </InfoText>
        </Container>
        </>
    )
}
