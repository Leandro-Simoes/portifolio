import React from 'react'
import { 
    HeaderStyled, 
    ContainerContent, 
    ContainerLinks, 
    ContainerMidias, 
    Content,
    ContentText,
    ContentImg,
    ContainerImg
} from './styles'

import Linkedin from '../../assets/linkedin.svg'
import Facebook from '../../assets/facebook.svg'
import Instagram from '../../assets/instagram.svg'
import ContainerSvg from '../ContainerSvg'
import Perfil from '../../assets/perfil.jpg'

export default function Header() {
    return (
        <HeaderStyled>
            <ContainerContent>
                <h1>LS</h1>
                <ContainerLinks>
                    <h4>Sobre</h4>
                    <h4>HardSkills</h4>
                    <h4>SoftSkills</h4>
                    <h4>Contato</h4>
                </ContainerLinks>
                <ContainerMidias>
                    <a href='https://www.linkedin.com/in/leandro-sim%C3%B5es/' target='_blank' rel="noreferrer" ><ContainerSvg svg={Linkedin} width={30} height={30}/></a>
                    <a><ContainerSvg svg={Facebook} width={30} height={30}/></a>
                    <a><ContainerSvg svg={Instagram} width={30} height={30}/></a>
                </ContainerMidias>
            </ContainerContent>
            <Content>
                <ContentText>
                    <h3>Portifolio</h3>
                    <h2>Olá, eu sou o Leandro Simões</h2>
                    <p>
                    Sou desenvolvedor front-end e estou cursando desenvolvedor Full-stack na Kenzie Academy Brasil
                    </p>
                </ContentText>
                <ContentImg>
                    <ContainerImg>
                        <img src={Perfil} alt="Perfil" />
                    </ContainerImg>
                </ContentImg>
            </Content>
        </HeaderStyled>
    )
}
