import styled from "styled-components";

export const HeaderStyled = styled.header `
    width: 100vw;
    max-height: 700px;
    background: var(--primary);
    border-radius: 0px 0px 100px 100px;
    h1{
        margin-left: 8vw;
        font-family: var(--fontLogo), sans-serif;
        font-size: 36px;
    }
    display: flex;
    flex-direction: column;
`

export const ContainerContent = styled.div `
    width: 100%;
    height: 90px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: var(--white);
`

export const ContainerLinks = styled.div `
    width: 40vw;
    height: 50%;
    display: flex;
    justify-content: space-between;
    align-items: center;
`

export const ContainerMidias = styled.div `
    width: 15vw;
    height: 50%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-right: 100px;
`

export const Content = styled.div `
    width: 100vw;
    height: calc(100vh - 90px);
    display: flex;
    justify-content: space-around;
`

export const ContentText = styled.div `
    min-width: 600px;
    height: calc(100vh - 90px);
    display: flex;
    flex-direction: column;
    h2 {
        color: var(--white);
        font-family: var(--fontTitle), sans-serif;
        font-size: 60px;
        text-align: left;
        width: 400px;
        margin-left: 8vw;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    h3{
        color: var(--white);
        text-align: left;
        margin-left: 8vw;
        font-family: var(--fontSubtitle), sans-serif;
        font-weight: 400;
        margin-top: 60px;
    }
    p{
        font-size: 20px;
        text-align: left;
        margin-left: 8vw;
        margin-top: 0;
        color: var(--greySubtitle);
        width: 300px;
    }

`

export const ContentImg = styled.div `
    min-width: 450px;
    height: calc(100vh - 90px);
    display: flex;
    justify-content: center;
`

export const ContainerImg = styled.div `
    width: 400px;
    height: 400px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    margin-top: 60px;
    border: 2px solid #c0e0de;
    box-shadow: 0 0 6px #c0e0de;
    img{
    width: 95%;
    height: 95%;
    border-radius: 50%;
    }

`

