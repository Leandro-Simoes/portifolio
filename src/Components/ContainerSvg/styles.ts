import styled, {css} from "styled-components";

interface svg {
    width: number;
    height: number;
    svg: string; 
}

export const Container = styled.div<svg>`
    ${(props) => 
        css `
        width: ${props.width}px;
        height: ${props.height}px;
        background: url(${props.svg});
        background-size: cover;
        `
    }


`