import React from 'react'
import { Container } from './styles'

interface svg {
    width: number;
    height: number;
    svg: string; 
}

export default function ContainerSvg({ svg, width, height }: svg) {
    return (
        <Container svg={svg} width={width} height={height}/>
    )
}
