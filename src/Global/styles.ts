import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
    :root{
        --primary: #5842BE;
        --grey: #EEEEEE;
        --yellow: #F9F8AE;
        --white: #fff;
        --black: #000;
        --greySubtitle: #ACA7A7;

        --fontTitle: 'Anton';
        --fontSubtitle: 'Roboto Mono';
        --fontLogo: 'Fredoka One'
    }
    body {
        margin: 0;
        padding: 0;
        background: white;
    }
    a{
        text-decoration: none;
    }
`

export default GlobalStyles